
import init from "./out/hexwarheads.js";

const runWasm = async () => {
  // Instantiate our wasm module
  await init("./out/hexwarheads_bg.wasm");
};
runWasm();