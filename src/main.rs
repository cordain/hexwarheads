mod map;
mod meshes;
mod states;
mod target;
mod tower;

use crate::states::TestPlayState;

use bevy::{prelude::*, render::camera::ScalingMode};
#[cfg(feature = "inspector")]
use bevy_inspector_egui::WorldInspectorPlugin;
use bevy_mod_picking::*;
use iyes_loopless::prelude::*;
use map::PoiMapPlugin;
use states::StateMachinePlugin;
use target::TargetPlugin;
use tower::TowerPlugin;

// Thanks to [Wolfram Alpha answer](https://www.wolframalpha.com/input?i=sqrt%283%29%2F2)

#[derive(Component)]
struct FpsMeter;
#[derive(Component)]
struct Help;

fn spawn_camera(mut commands: Commands) {
   commands
      .spawn(Camera3dBundle {
         projection: OrthographicProjection {
            scale: 17.0,
            scaling_mode: ScalingMode::FixedVertical(2.0),
            ..default()
         }
         .into(),
         transform: Transform::from_xyz(0.0, 10.0, 6.0).looking_at(Vec3::ZERO, -Vec3::Z),
         ..default()
      })
      .insert(PickingCameraBundle::default())
      .insert(Name::new("Camera"));
   commands
      .spawn(DirectionalLightBundle {
         directional_light: DirectionalLight {
            illuminance: 1500.0,
            shadows_enabled: true,
            ..default()
         },
         transform: Transform::from_xyz(0.0, 10.0, -6.0).looking_at(Vec3::ZERO, -Vec3::Z),
         ..default()
      })
      .insert(Name::new("Light"));
}

fn handle_button(
   mut requested_state: Query<&mut TestPlayState>,
   game_state: Res<CurrentState<TestPlayState>>,
   interaction: Query<(&Interaction, &Children), Changed<Interaction>>,
   mut text: Query<&mut Text, Without<FpsMeter>>,
) {
   for (interaction, children) in &interaction {
      if *interaction == Interaction::Clicked {
         //Request the next state
         let mut next_state = game_state.0.clone();
         next_state.next();
         *requested_state.get_single_mut().unwrap() = next_state;

         //modify text
         let mut text = text.get_mut(children[0]).unwrap();
         text.sections[0].value = "Accept".to_string();
      }
   }
}

fn spawn_button(mut commands: Commands, asset_server: Res<AssetServer>) {
   commands
      .spawn(ButtonBundle {
         style: Style {
            size: Size::new(Val::Px(150.0), Val::Px(65.0)),
            margin: UiRect::all(Val::Auto),
            justify_content: JustifyContent::Center,
            align_items: AlignItems::Center,
            position: UiRect {
               bottom: Val::Percent(-44.0),
               ..default()
            },
            ..default()
         },
         background_color: Color::rgb(0.0, 0.0, 0.0).into(),
         ..default()
      })
      .with_children(|commands| {
         commands.spawn(TextBundle::from_section(
            "Begin",
            TextStyle {
               font: asset_server.load("fonts/FiraSans-Medium.ttf"),
               font_size: 40.0,
               color: Color::rgb(0.9, 0.9, 0.9),
            },
         ));
      });
}

fn spawn_fps_meter(mut commands: Commands, asset_server: Res<AssetServer>) {
   commands
      .spawn(
         TextBundle::from_section(
            "0.0 fps",
            TextStyle {
               font: asset_server.load("fonts/FiraSans-Medium.ttf"),
               font_size: 16.0,
               color: Color::rgb(0.9, 0.9, 0.9),
            },
         )
         .with_style(Style {
            position_type: PositionType::Absolute,
            size: Size::new(Val::Auto, Val::Auto),
            align_items: AlignItems::Center,
            position: UiRect {
               top: Val::Percent(0.0),
               right: Val::Percent(3.5),
               ..default()
            },
            ..default()
         }),
      )
      .insert(FpsMeter);
}

fn spawn_help(mut commands: Commands, asset_server: Res<AssetServer>) {
   commands
      .spawn(
         TextBundle::from_section(
            "",
            TextStyle {
               font: asset_server.load("fonts/FiraSans-Medium.ttf"),
               font_size: 15.0,
               color: Color::rgb(0.9, 0.9, 0.9),
            },
         )
         .with_style(Style {
            position_type: PositionType::Absolute,
            size: Size::new(Val::Auto, Val::Auto),
            align_items: AlignItems::Center,
            position: UiRect {
               top: Val::Percent(3.0),
               left: Val::Percent(2.8),
               ..default()
            },
            ..default()
         }),
      )
      .insert(Help);
}

fn tick_help(
   mut text: Query<&mut Text, With<Help>>,
   current_state: Res<CurrentState<TestPlayState>>,
) {
   if let Ok(mut text) = text.get_single_mut() {
      let (begin_done, tower_place_done, path_select_done, wave_done) = match current_state.0 {
         TestPlayState::Init => (" ", " ", " ", " "),
         TestPlayState::PlaceTowers => ("x", " ", " ", " "),
         TestPlayState::SelectAttackerCheckpoints => ("x", "x", " ", " "),
         TestPlayState::LaunchWave => ("x", "x", "x", " "),
         TestPlayState::End => ("x", "x", "x", "x"),
      };
      text.sections[0].value = format!(
         "
Welcome to the Hexwarheads prototype.

What you can see here is map of single tile of the world map.
World map will be added in the future.

This map contains pathfinding tiles of different colors:
Light green: Point of interest to be defended, destinations for the attacking wave
Red: Possible spawning locations for the wave.
Green: Preferrable tiles on which wave can travel. On these towers can be placed.
Yellow: Slow tiles on which wave can travel.
Rest: Inaccessible tiles.

To play on this prototype map you need do follow these steps:
[{}] Click button on the bottom to begin sequence. 
[{}] Place 3 towers on green tiles and press the button. 
[{}] Select wave travel path by selecting in order:
      -  Spawn (red) tile
      -  3 Poi (Light green) tiles placed around the center
      -  Center Poi (Light green) tile
     then press the button
[{}] Enjoy the battle
",
         begin_done, tower_place_done, path_select_done, wave_done
      );
   }
}

fn handle_fps_meter(mut text: Query<&mut Text, With<FpsMeter>>, time: Res<Time>) {
   if let Ok(mut text) = text.get_single_mut() {
      text.sections[0].value = format!("{:.0} FPS", 1.0 / time.delta_seconds());
   }
}

#[cfg(feature = "inspector")]
fn add_inspector_plugin(app: &mut App) {
   app.add_plugin(WorldInspectorPlugin::new());
}

fn main() {
   let mut app = App::new();
   app.insert_resource(ClearColor(Color::rgb(0.2, 0.2, 0.2)))
      .add_plugins(DefaultPickingPlugins)
      .add_startup_system(spawn_camera)
      .add_startup_system(spawn_button)
      .add_startup_system(spawn_fps_meter)
      .add_startup_system(spawn_help)
      .add_plugin(StateMachinePlugin)
      .add_plugins(DefaultPlugins.set(WindowPlugin {
         window: WindowDescriptor {
            title: "Bevy Tower Defense".to_string(),
            resizable: false,
            ..Default::default()
         },
         ..default()
      }))
      .add_plugin(bevy_web_resizer::Plugin)
      .add_plugin(PoiMapPlugin)
      //.add_system(camera_controls)
      .add_system(handle_button)
      .add_plugin(TargetPlugin)
      .add_plugin(TowerPlugin)
      .add_system(handle_fps_meter)
      .add_system(tick_help);
   #[cfg(feature = "inspector")]
   add_inspector_plugin(&mut app);
   app.run();
}
