use std::f32::consts::{FRAC_PI_3, FRAC_PI_6};

use bevy::{
   prelude::Mesh,
   render::{mesh::Indices, render_resource::PrimitiveTopology},
};
/// A hexagon on the `XZ` plane centered at the origin.
#[derive(Debug, Copy, Clone)]
pub struct HexPlane {
   /// The edge length.
   pub edge: f32,
}

impl Default for HexPlane {
   fn default() -> Self {
      HexPlane { edge: 1.0 }
   }
}

impl From<HexPlane> for Mesh {
   fn from(hexagon: HexPlane) -> Self {
      let mut positions: Vec<_> = (0..6)
         .map(|fact_pi| {
            [
               hexagon.edge * (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).cos(),
               0.0,
               hexagon.edge * (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).sin(),
            ]
         })
         .collect();
      positions.push([0.0, 0.0, 0.0]);
      let normals: Vec<_> = (0..7).map(|_| [0.0, 1.0, 0.0]).collect();
      let mut uvs: Vec<_> = (0..6)
         .map(|fact_pi| {
            [
               (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).cos() / 2.0 + 0.5,
               (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).sin() / 2.0 + 0.5,
            ]
         })
         .collect();
      uvs.push([0.5, 0.5]);

      let indices = Indices::U32(vec![0, 6, 1, 1, 6, 2, 2, 6, 3, 3, 6, 4, 4, 6, 5, 5, 6, 0]);

      let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
      mesh.set_indices(Some(indices));
      mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
      mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
      mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
      mesh
   }
}

#[derive(Debug, Copy, Clone)]
pub struct HexColumn {
   /// The edge length.
   pub edge: f32,
   pub height: f32,
}

impl Default for HexColumn {
   fn default() -> Self {
      HexColumn {
         edge: 1.0,
         height: 0.2,
      }
   }
}

impl From<HexColumn> for Mesh {
   fn from(hexagon: HexColumn) -> Self {
      let positions = {
         let mut positions: Vec<_> = (0..6)
            .map(|fact_pi| {
               [
                  hexagon.edge * (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).cos(),
                  0.0,
                  hexagon.edge * (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).sin(),
               ]
            })
            .collect();
         positions.push([0.0, 0.0, 0.0]);
         (0..6)
            .map(|fact_pi| {
               [
                  hexagon.edge * (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).cos(),
                  hexagon.height,
                  hexagon.edge * (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).sin(),
               ]
            })
            .for_each(|vert| positions.push(vert.to_owned()));
         positions.push([0.0, hexagon.height, 0.0]);
         positions
      };
      let normals: Vec<_> = (0..14).map(|_| [0.0, 1.0, 0.0]).collect();
      let uvs: Vec<_> = (0..14)
         .map(|fact_pi| {
            [
               (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).cos() / 2.0 + (fact_pi as f32) * 0.5 / 14.0,
               (FRAC_PI_3 * fact_pi as f32 + FRAC_PI_6).sin() / 2.0 + (fact_pi as f32) * 0.5 / 14.0,
            ]
         })
         .collect();

      let indices = Indices::U32(vec![
         0, 6, 1, 1, 6, 2, 2, 6, 3, 3, 6, 4, 4, 6, 5, 5, 6, 0, 0, 1, 8, 8, 7, 0, 1, 2, 9, 9, 8, 1,
         2, 3, 10, 10, 9, 2, 3, 4, 11, 11, 10, 3, 4, 5, 12, 12, 11, 4, 5, 0, 7, 7, 12, 5, 7, 13, 8,
         8, 13, 9, 9, 13, 10, 10, 13, 11, 11, 13, 12, 12, 13, 7,
      ]);

      let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
      mesh.set_indices(Some(indices));
      mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
      mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
      mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
      mesh
   }
}
