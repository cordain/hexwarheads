use crate::tower::TowerCount;
use std::collections::HashMap;

use bevy::prelude::*;
use iyes_loopless::prelude::*;

use crate::map::{
   check_select_attacker_checkpoint_conditions, generators::poi::PoiTileState, PathfindMarker,
};
#[derive(Component, Default, Hash, Debug, Eq, Clone, PartialEq)]
pub enum TestPlayState {
   #[default]
   Init,
   PlaceTowers,
   SelectAttackerCheckpoints,
   LaunchWave,
   End,
}

impl TestPlayState {
   pub fn next(&mut self) {
      *self = match self {
         Self::Init => Self::PlaceTowers,
         Self::PlaceTowers => Self::SelectAttackerCheckpoints,
         Self::SelectAttackerCheckpoints => Self::LaunchWave,
         Self::LaunchWave => Self::End,
         Self::End => Self::End,
      };
   }
}

pub struct StateMachinePlugin;

fn state_transition(
   mut commands: Commands,
   state: Res<CurrentState<TestPlayState>>,
   map_data: Query<(&TestPlayState, &TowerCount), Changed<TestPlayState>>,
   tile_data: Query<&PathfindMarker, With<PoiTileState>>,
) {
   if let Ok((requested_state, tower_count)) = map_data.get_single() {
      if state.0 != *requested_state {
         match requested_state {
            TestPlayState::Init => true,
            TestPlayState::PlaceTowers => true,
            TestPlayState::SelectAttackerCheckpoints => tower_count.0 == 3,
            TestPlayState::LaunchWave => {
               let mut occured_once = HashMap::<PathfindMarker, bool>::new();
               for marker in &tile_data {
                  occured_once
                     .entry(marker.to_owned())
                     .and_modify(|condition| {
                        *condition = false;
                     })
                     .or_insert(true);
               }
               info!("{:?}", occured_once);
               check_select_attacker_checkpoint_conditions(&occured_once)
            }
            TestPlayState::End => false,
         }
         .then(|| commands.insert_resource(NextState(requested_state.to_owned())));
      }
   }
}

impl Plugin for StateMachinePlugin {
   fn build(&self, app: &mut bevy::prelude::App) {
      app.add_loopless_state(TestPlayState::default())
         .add_system(state_transition);
   }
}
