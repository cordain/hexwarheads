use crate::{
   map::Path,
   meshes::HexPlane,
   target::{Health, Speed, TravelToPoint},
};
use bevy::{prelude::*, utils::FloatOrd};
use iyes_loopless::prelude::*;

use crate::{meshes::HexColumn, states::TestPlayState};

#[derive(Component)]
pub struct TowerCount(pub u8);
#[derive(Component)]
pub struct Tower {
   bullet_template: Bullet,
   range: f32,
}
#[derive(Component, Clone)]
pub struct Bullet {
   speed: f32,
   damage: f32,
   target: Entity,
}
impl Bullet {
   pub fn with_target(mut self, target: Entity) -> Self {
      self.target = target;
      self
   }
}
#[derive(Component)]
pub struct TowerTimer {
   fire_timer: Timer,
}
#[derive(Component)]
pub enum TowerType {
   Normal,
   Fast,
   Sniper,
}

impl TowerType {
   pub fn get_tower(&self) -> Tower {
      match self {
         TowerType::Normal => Tower {
            bullet_template: Bullet {
               speed: 5.0,
               damage: 1.0,
               target: Entity::from_raw(0),
            },
            range: 3.0,
         },
         TowerType::Fast => Tower {
            bullet_template: Bullet {
               speed: 4.0,
               damage: 0.5,
               target: Entity::from_raw(0),
            },
            range: 2.0,
         },
         TowerType::Sniper => Tower {
            bullet_template: Bullet {
               speed: 10.0,
               damage: 3.0,
               target: Entity::from_raw(0),
            },
            range: 5.0,
         },
      }
   }
   pub fn get_tower_firerate(&self) -> TowerTimer {
      match self {
         TowerType::Normal => TowerTimer {
            fire_timer: Timer::from_seconds(1.0, TimerMode::Once),
         },
         TowerType::Fast => TowerTimer {
            fire_timer: Timer::from_seconds(0.8, TimerMode::Once),
         },
         TowerType::Sniper => TowerTimer {
            fire_timer: Timer::from_seconds(3.0, TimerMode::Once),
         },
      }
   }
}

fn spawn_tower(
   mut commands: Commands,
   mut meshes: ResMut<Assets<Mesh>>,
   mut materials: ResMut<Assets<StandardMaterial>>,
   tower: Query<(Entity, &TowerType), Added<TowerType>>,
) {
   for (entity, towertype) in &tower {
      commands.entity(entity).with_children(|commands| {
         commands
            .spawn(PbrBundle {
               mesh: meshes.add(Mesh::from(HexColumn {
                  edge: 0.5,
                  height: 1.0,
               })),
               material: materials.add(Color::rgba(1.0, 1.0, 1.0, 1.0).into()),
               ..default()
            })
            .insert(towertype.get_tower())
            .insert(towertype.get_tower_firerate());
      });
   }
}

fn shoot_tower(
   mut commands: Commands,
   targets: Query<(Entity, &GlobalTransform), (With<Speed>, With<Path>, With<TravelToPoint>)>,
   mut towers: Query<(&GlobalTransform, &Tower, &mut TowerTimer)>,
   mut materials: ResMut<Assets<StandardMaterial>>,
   mut meshes: ResMut<Assets<Mesh>>,
   time: Res<Time>,
) {
   for (tower_transform, tower_data, mut fire_timer) in &mut towers {
      fire_timer.fire_timer.tick(time.delta());
      if fire_timer.fire_timer.finished() {
         if let Some((entity, target_transform)) = targets.iter().min_by_key(|(_, transform)| {
            FloatOrd(
               tower_transform
                  .translation()
                  .distance(transform.translation()),
            )
         }) {
            //crude example of shooting, it shoots to all targets in range
            if tower_transform
               .translation()
               .distance(target_transform.translation())
               < tower_data.range
            {
               commands
                  .spawn(PbrBundle {
                     mesh: meshes.add(HexPlane { edge: 0.25 }.into()),
                     material: materials.add(Color::rgb(0.0, 0.0, 0.0).into()),
                     transform: Transform::from_translation(tower_transform.translation()),
                     ..default()
                  })
                  .insert(tower_data.bullet_template.to_owned().with_target(entity));
            }
         }
         fire_timer.fire_timer.reset();
      }
   }
}

fn move_bullet(
   mut commands: Commands,
   mut targets: Query<(&Transform, &mut Health), (With<Speed>, With<Path>, With<TravelToPoint>)>,
   mut bullets: Query<
      (Entity, &mut Transform, &Bullet),
      (Without<Speed>, Without<Path>, Without<TravelToPoint>),
   >,
   time: Res<Time>,
) {
   for (entity, mut bullet_transform, bullet_data) in &mut bullets {
      if let Ok((target_transform, mut health)) = targets.get_mut(bullet_data.target) {
         let direction = (target_transform.translation - bullet_transform.translation).normalize();
         bullet_transform.translation += direction * bullet_data.speed * time.delta_seconds();
         if target_transform
            .translation
            .distance(bullet_transform.translation)
            < 0.2
         {
            health.0 -= bullet_data.damage;
            commands.entity(entity).despawn_recursive()
         }
      }
   }
}

pub struct TowerPlugin;

impl Plugin for TowerPlugin {
   fn build(&self, app: &mut App) {
      app.add_system(spawn_tower.run_in_state(TestPlayState::PlaceTowers))
         .add_system(shoot_tower.run_in_state(TestPlayState::LaunchWave))
         .add_system(move_bullet.run_in_state(TestPlayState::LaunchWave));
   }
}
