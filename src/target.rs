use bevy::prelude::{shape::Cube, *};
use iyes_loopless::prelude::*;

use crate::{
   map::{generators::Idx2d, Path, PathfindMarker, PoiMap},
   states::TestPlayState,
};

#[derive(SystemLabel)]
enum HandleTargetsSequence {
   DespawnTargets,
   SpawnTargets,
   MoveTargets,
}

#[derive(Default)]
enum TargetType {
   #[default]
   Normal,
   Fast,
   Tough,
}

impl TargetType {
   pub fn into_params(self) -> (Health, Speed) {
      match self {
         TargetType::Normal => (Health(3.0), Speed(2.5)),
         TargetType::Fast => (Health(2.0), Speed(3.0)),
         TargetType::Tough => (Health(6.0), Speed(2.0)),
      }
   }
}

#[derive(Component)]
pub struct TravelToPoint(Vec3);

#[derive(Component)]
pub struct Health(pub f32);
#[derive(Component)]
pub struct Speed(f32);
#[derive(Component)]
struct Wave(Vec<TargetType>);
#[derive(Component)]
struct WaveCooldown(Timer);

pub struct TargetPlugin;

impl Plugin for TargetPlugin {
   fn build(&self, app: &mut bevy::prelude::App) {
      app.add_system(
         despawn_targets
            .run_in_state(TestPlayState::LaunchWave)
            .label(HandleTargetsSequence::DespawnTargets)
            .before(HandleTargetsSequence::SpawnTargets),
      )
      .add_system(
         spawn_targets
            .run_in_state(TestPlayState::LaunchWave)
            .label(HandleTargetsSequence::SpawnTargets)
            .before(HandleTargetsSequence::MoveTargets),
      )
      .add_system(
         move_targets
            .run_in_state(TestPlayState::LaunchWave)
            .label(HandleTargetsSequence::MoveTargets),
      )
      .add_enter_system(TestPlayState::LaunchWave, insert_test_wave);
   }
}

fn insert_test_wave(mut commands: Commands, mut map: Query<Entity, With<PoiMap>>) {
   let wave = Wave(
      (0..10)
         .rev()
         .map(|x| {
            if x % 5 == 0 {
               TargetType::Tough
            } else if x % 2 == 0 {
               TargetType::Fast
            } else {
               TargetType::Normal
            }
         })
         .collect(),
   );
   commands
      .entity(map.get_single_mut().unwrap())
      .insert(wave)
      .insert(WaveCooldown(Timer::from_seconds(1.0, TimerMode::Once)));
}
fn despawn_targets(
   mut commands: Commands,
   mut base_health: Query<&mut Health, (With<PoiMap>, Without<Speed>)>,
   targets: Query<(Entity, &Health, &Transform), With<Speed>>,
) {
   for (entity, health, transform) in &targets {
      if transform.translation.distance(Vec3::ZERO) < 0.3 {
         base_health.get_single_mut().unwrap().0 -= 1.0;
         commands.entity(entity).despawn_recursive();
      }
      if health.0 <= 0.0 {
         commands.entity(entity).despawn_recursive();
      }
   }
}
fn spawn_targets(
   mut commands: Commands,
   mut map: Query<(&mut Wave, &mut WaveCooldown, &Path), With<PoiMap>>,
   mut meshes: ResMut<Assets<Mesh>>,
   mut materials: ResMut<Assets<StandardMaterial>>,
   time: Res<Time>,
) {
   let (mut wave, mut cooldown, path) = map.get_single_mut().unwrap();
   (cooldown).0.tick(time.delta());
   if cooldown.0.finished() {
      if let Some(target_type) = wave.0.pop() {
         let (health, speed) = target_type.into_params();
         let mut private_path = path.clone();
         let spawn_pos = private_path.0.pop().unwrap();
         let first_tile = private_path.0.pop().unwrap();
         commands
            .spawn(PbrBundle {
               mesh: meshes.add(Mesh::from(Cube { size: 0.5 })),
               material: materials.add(Color::rgba(1.0, 0.0, 0.0, 1.0).into()),
               transform: Transform::from_translation(spawn_pos),
               ..default()
            })
            .insert(health)
            .insert(speed)
            .insert(private_path)
            .insert(TravelToPoint(first_tile));
      }
      cooldown.0.reset();
   }
}
fn move_targets(
   mut targets: Query<(&mut Transform, &Speed, &mut Path, &mut TravelToPoint)>,
   time: Res<Time>,
) {
   for (mut transform, speed, mut path, mut travel_to_point) in &mut targets {
      if transform.translation.distance(travel_to_point.0) < 0.2 {
         if let Some(translation) = path.0.pop() {
            info!("new destination: {:?}", translation);
            travel_to_point.0 = translation;
         }
      }
      let direction = (travel_to_point.0 - transform.translation).normalize();
      transform.translation += direction * speed.0 * time.delta_seconds()
   }
}
