pub mod generators;
use iyes_loopless::prelude::*;

use std::collections::HashMap;

use crate::{
   map::generators::FloatOrdZero,
   meshes::{HexColumn, HexPlane},
   states::TestPlayState,
   target::Health,
   tower::{TowerCount, TowerType},
};
use bevy::{pbr::NotShadowCaster, prelude::*};
use bevy_mod_picking::*;
use generators::{
   poi::{PoiBuilder, PoiTileState},
   Idx2d,
};
use pathfinding::directed::astar::astar;

use self::generators::{NeighbourIndexes, WaveState};

#[derive(Component, Default, Eq, Hash, PartialEq, Clone, Debug)]
pub enum PathfindMarker {
   #[default]
   NoPathfind,
   PathfindStart,
   PathfindCheckpoint1,
   PathfindCheckpoint2,
   PathfindCheckpoint3,
   PathfindEnd,
}
impl PathfindMarker {
   pub fn next(&mut self) {
      *self = match self {
         Self::NoPathfind => Self::NoPathfind,
         Self::PathfindStart => Self::PathfindCheckpoint1,
         Self::PathfindCheckpoint1 => Self::PathfindCheckpoint2,
         Self::PathfindCheckpoint2 => Self::PathfindCheckpoint3,
         Self::PathfindCheckpoint3 => Self::PathfindEnd,
         Self::PathfindEnd => Self::NoPathfind,
      };
   }
   pub fn into_vec() -> Vec<Self> {
      vec![
         Self::PathfindStart,
         Self::PathfindCheckpoint1,
         Self::PathfindCheckpoint2,
         Self::PathfindCheckpoint3,
         Self::PathfindEnd,
      ]
   }
}

#[derive(Component)]
pub struct PoiMap;
#[derive(Component, Clone)]
pub struct Path(pub Vec<Vec3>);

pub struct PoiMapPlugin;

fn assign_pathfind_points(
   mut commands: Commands,
   poi_specials: Query<(Entity, &PoiTileState), Added<PoiTileState>>,
) {
   info!("hello pathfinds");
   for (entity, state) in &poi_specials {
      if state.is_pathfind_target() {
         commands.entity(entity).insert(PathfindMarker::default());
      }
   }
}

fn calculate_pathfind(
   mut commands: Commands,
   tiles: Query<(
      &PoiTileState,
      &Idx2d,
      &NeighbourIndexes,
      &Transform,
      Option<&PathfindMarker>,
      Option<&TowerType>,
   )>,
   mut map: Query<Entity, With<TestPlayState>>,
) {
   let mut astar_ready_data = HashMap::<Idx2d, (PoiTileState, NeighbourIndexes, Vec3, bool)>::new();
   let mut astar_ready_targets = HashMap::<PathfindMarker, Idx2d>::new();
   info!("Let's send some bad stuff");
   for (tile_state, idx2d, neighbours, transform, pathfind_marker, tower_type) in &tiles {
      astar_ready_data.insert(
         idx2d.to_owned(),
         (
            tile_state.to_owned(),
            neighbours.to_owned(),
            transform.translation,
            tower_type.is_some(), // simply if there is a tower
         ),
      );
      if let Some(marker) = pathfind_marker {
         astar_ready_targets.insert(marker.to_owned(), idx2d.to_owned());
      }
   }
   commands.entity(map.get_single_mut().unwrap()).insert(Path(
      //let mut paths = Vec::new();
      PathfindMarker::into_vec()
         .windows(2)
         .map(|from_to| {
            let from = &from_to[0];
            let to = &from_to[1];
            astar(
               &astar_ready_targets[&from],
               |idx2d| {
                  astar_ready_data[idx2d]
                     .1
                      .0
                     .iter()
                     .map(|&neighbour| {
                        (
                           neighbour,
                           if astar_ready_data[&neighbour].3 {
                              f32::INFINITY.into()
                           } else {
                              PoiTileState::get_cost(&astar_ready_data[&neighbour].0)
                           },
                        )
                     })
                     .collect::<Vec<(Idx2d, FloatOrdZero)>>()
               },
               |idx2d| {
                  astar_ready_data[idx2d]
                     .2
                     .distance(astar_ready_data[&astar_ready_targets[&to]].2)
                     .into()
               },
               |&idx2d| idx2d == astar_ready_targets[&to],
            )
            .unwrap()
            .0
            .into_iter()
            .map(|idx2d| astar_ready_data[&idx2d].2)
            .collect::<Vec<Vec3>>()
         })
         .flatten()
         .rev() //to pop the next tile checkpoint
         .collect(),
   ));
}

fn handle_selection(
   mut commands: Commands,
   game_state: Res<CurrentState<TestPlayState>>,
   mut map: Query<(Entity, &mut TowerCount, &mut PathfindMarker)>,
   mut tiles: Query<
      (
         Entity,
         &PoiTileState,
         &mut Children,
         Option<&mut PathfindMarker>,
      ),
      (Without<TowerType>, Without<TowerCount>),
   >,
   mut selection: Query<(Entity, &mut Selection), Changed<Selection>>,
) {
   for (_, mut tower_count, mut next_marker) in &mut map {
      for (parent, state, children, pathfind_marker) in &mut tiles {
         for &child in children.iter() {
            if let Ok((_, mut selection)) = selection.get_mut(child) {
               if selection.selected() {
                  match game_state.0 {
                     TestPlayState::PlaceTowers => {
                        if tower_count.0 < 3 && state.is_placeable() {
                           commands.entity(parent).insert(TowerType::Normal);
                           tower_count.0 += 1;
                           break;
                        }
                     }
                     TestPlayState::SelectAttackerCheckpoints => {
                        if pathfind_marker.is_some() {
                           let assign_condition = match *next_marker {
                              PathfindMarker::PathfindStart => *state == PoiTileState::Spawn,
                              PathfindMarker::PathfindCheckpoint1 => {
                                 *state == PoiTileState::PoiMinor
                              }
                              PathfindMarker::PathfindCheckpoint2 => {
                                 *state == PoiTileState::PoiMinor
                              }
                              PathfindMarker::PathfindCheckpoint3 => {
                                 *state == PoiTileState::PoiMinor
                              }
                              PathfindMarker::PathfindEnd => *state == PoiTileState::PoiMajor,
                              _ => false,
                           };
                           if assign_condition {
                              info!("Selected good for {:?}", next_marker);
                              *pathfind_marker.unwrap() = next_marker.to_owned();
                              next_marker.next();
                              break;
                           }
                        }
                     }
                     _ => {}
                  }
                  selection.set_selected(false);
               }
            }
         }
      }
   }
}

fn spawn_poi_scene(
   mut commands: Commands,
   mut meshes: ResMut<Assets<Mesh>>,
   mut materials: ResMut<Assets<StandardMaterial>>,
   asset_server: Res<AssetServer>,
) {
   let my_contour = asset_server.load("textures/tile_contour.png");
   commands
      .spawn(SpatialBundle::from_transform(Transform::default()))
      .insert(PoiMap)
      .insert(TowerCount(0))
      .insert(TestPlayState::default())
      .insert(PathfindMarker::PathfindStart)
      .insert(Health(1.0))
      .with_children(|commands| {
         PoiBuilder::new()
            .with_map_size(5)
            .pre_generate()
            .generate()
            .verify()
            .build()
            .iter()
            .for_each(|(idx2d, tile)| {
               let default_collider_color = materials.add(Color::rgba(0.0, 0.0, 0.0, 0.0).into());
               let pressed_collider_color = materials.add(Color::rgba(1.0, 1.0, 1.0, 0.2).into());
               let selected_collider_color = materials.add(Color::rgba(1.0, 1.0, 1.0, 0.3).into());
               commands
                  .spawn(SpatialBundle::from_transform(Transform::from_translation(
                     tile.translation(),
                  )))
                  .insert(idx2d.to_owned())
                  .insert(tile.neighbours())
                  .insert(tile.state())
                  .with_children(|commands| {
                     commands.spawn(PbrBundle {
                        mesh: meshes.add(Mesh::from(HexPlane { edge: 1.0 })),
                        material: materials.add(StandardMaterial {
                           base_color_texture: Some(my_contour.clone()),
                           base_color: tile.state().get_asset_data().into(),
                           alpha_mode: AlphaMode::Blend,
                           unlit: true,
                           ..default()
                        }),
                        ..default()
                     });
                     commands
                        .spawn(SpatialBundle::default())
                        .insert(meshes.add(HexColumn::default().into()))
                        .insert(Highlighting {
                           initial: default_collider_color.clone(),
                           hovered: Some(selected_collider_color.clone()),
                           pressed: Some(pressed_collider_color),
                           selected: Some(selected_collider_color),
                        })
                        .insert(default_collider_color)
                        .insert(PickableBundle::default())
                        .insert(NotShadowCaster);
                  })
                  .insert(Name::new(format!("{}", idx2d)));
            })
      });
}

pub fn check_select_attacker_checkpoint_conditions(
   present_once: &HashMap<PathfindMarker, bool>,
) -> bool {
   present_once
      .get(&PathfindMarker::PathfindStart)
      .unwrap_or(&false)
      .to_owned()
      && present_once
         .get(&PathfindMarker::PathfindCheckpoint1)
         .unwrap_or(&false)
         .to_owned()
      && present_once
         .get(&PathfindMarker::PathfindCheckpoint2)
         .unwrap_or(&false)
         .to_owned()
      && present_once
         .get(&PathfindMarker::PathfindCheckpoint3)
         .unwrap_or(&false)
         .to_owned()
      && present_once
         .get(&PathfindMarker::PathfindEnd)
         .unwrap_or(&false)
         .to_owned()
}

#[derive(SystemLabel)]
enum PoiBuildSystem {
   GenerateMap,
   MarkPathfinds,
}

impl Plugin for PoiMapPlugin {
   fn build(&self, app: &mut bevy::prelude::App) {
      app.add_enter_system(TestPlayState::Init, spawn_poi_scene)
         .add_exit_system(TestPlayState::Init, assign_pathfind_points)
         .add_exit_system(TestPlayState::SelectAttackerCheckpoints, calculate_pathfind)
         .add_system(handle_selection);
   }
}
