use std::collections::HashMap;

use bevy::prelude::{info, Color, Component, Vec3};
use bevy::utils::FloatOrd;
use pathfinding::directed::astar::astar;
use rand::{rngs::ThreadRng, RngCore};

use super::{wave_function_collapse, FloatOrdZero, Idx2d, NEIGHBOUR_LOGICAL_POS_OFFSETS};

use super::{fix_wave_errors, Tile, WaveState, FRAC_SQRT_3_2};

const POI_MAP_TOWN_INDEX: Idx2d = Idx2d(0, 0);
//          126,127,128,129,130,
//        74 ,                79 ,
//      112,                    118,
//    61 ,                        68 ,
//  98 ,          /*102*/           106,
//    49 ,                        56 ,
//      86 ,                    92 ,
//        38 ,                43 ,
//          26 ,27 ,28 ,29 ,30 ,
const POI_MAP_SPECIALS_POSSIBLE_INDEXES: [Idx2d; 24] = [
   Idx2d(-4, 8),
   Idx2d(-2, 8),
   Idx2d(0, 8),
   Idx2d(2, 8),
   Idx2d(4, 8),
   Idx2d(5, 6),
   Idx2d(6, 4),
   Idx2d(7, 2),
   Idx2d(8, 0),
   Idx2d(7, -2),
   Idx2d(6, -4),
   Idx2d(5, -6),
   Idx2d(4, -8),
   Idx2d(2, -8),
   Idx2d(0, -8),
   Idx2d(-2, -8),
   Idx2d(-4, -8),
   Idx2d(-5, -6),
   Idx2d(-6, -4),
   Idx2d(-7, -2),
   Idx2d(-8, 0),
   Idx2d(-7, 2),
   Idx2d(-6, 4),
   Idx2d(-5, 6),
];
const POI_MAP_SPAWN_POINTS: [Idx2d; 6] = [
   Idx2d(0, -16),
   Idx2d(-12, -8),
   Idx2d(12, -8),
   Idx2d(12, 8),
   Idx2d(-12, 8),
   Idx2d(0, 16),
];

#[derive(Default, Clone, PartialEq, Debug, Copy, Hash, Eq, Component)]
pub enum PoiTileState {
   SolidReachable,
   LooseReachable,
   Liquid, //Unreachable
   SolidUnreachable,
   LooseUnreachable,
   Pitfall,

   Spawn,
   PoiMinor,
   PoiMajor,
   #[default]
   NoState,
   ErrorState,
}

impl PoiTileState {
   pub fn get_asset_data(&self) -> Color {
      match self {
         PoiTileState::ErrorState => Color::rgba(0.0, 0.0, 0.0, 0.0),
         PoiTileState::SolidReachable => Color::rgb(45.0 / 255.0, 155.0 / 255.0, 45.0 / 255.0),
         PoiTileState::LooseReachable => Color::rgb(249.0 / 255.0, 1.0, 63.0 / 255.0),
         PoiTileState::Liquid => Color::rgb(55.0 / 255.0, 70.0 / 255.0, 233.0 / 255.0),
         PoiTileState::SolidUnreachable => Color::rgb(26.0 / 255.0, 41.0 / 255.0, 23.0 / 255.0),
         PoiTileState::LooseUnreachable => Color::rgb(0.5, 0.0, 0.0),
         PoiTileState::Pitfall => Color::rgb(0.1, 0.1, 0.1),
         PoiTileState::Spawn => Color::rgb(1.0, 0.0, 0.0),
         PoiTileState::PoiMinor => Color::rgb(97.0 / 255.0, 1.0, 114.0 / 255.0),
         PoiTileState::PoiMajor => Color::rgb(97.0 / 255.0, 1.0, 114.0 / 255.0),
         PoiTileState::NoState => Color::rgb(1.0, 1.0, 1.0),
      }
   }

   pub fn is_placeable(&self) -> bool {
      if *self == PoiTileState::SolidReachable {
         true
      } else {
         false
      }
   }

   pub fn is_pathfind_target(&self) -> bool {
      match self {
         PoiTileState::Spawn => true,
         PoiTileState::PoiMinor => true,
         PoiTileState::PoiMajor => true,
         _ => false,
      }
   }
}

impl WaveState for PoiTileState {
   fn possible_neighbour_states(state: &Self) -> Vec<PoiTileState> {
      match state {
         PoiTileState::SolidReachable => vec![
            PoiTileState::SolidReachable,
            PoiTileState::LooseReachable,
            PoiTileState::SolidUnreachable,
            PoiTileState::Pitfall,
         ],
         PoiTileState::LooseReachable => vec![
            PoiTileState::LooseReachable,
            PoiTileState::SolidReachable,
            PoiTileState::Liquid,
         ],
         PoiTileState::Liquid => vec![
            PoiTileState::LooseReachable,
            PoiTileState::LooseUnreachable,
            PoiTileState::Liquid,
         ],
         PoiTileState::SolidUnreachable => {
            vec![PoiTileState::SolidReachable, PoiTileState::SolidUnreachable]
         }
         PoiTileState::LooseUnreachable => {
            vec![PoiTileState::LooseReachable, PoiTileState::LooseUnreachable]
         }
         PoiTileState::Pitfall => vec![PoiTileState::SolidReachable, PoiTileState::Pitfall],
         PoiTileState::Spawn => vec![PoiTileState::SolidReachable],
         PoiTileState::PoiMinor => vec![PoiTileState::SolidReachable],
         PoiTileState::PoiMajor => vec![PoiTileState::SolidReachable],
         PoiTileState::NoState => vec![],
         PoiTileState::ErrorState => vec![],
      }
   }

   fn is_observed(state: &Self) -> bool {
      *state != PoiTileState::NoState
   }

   fn error_state() -> Self {
      PoiTileState::ErrorState
   }

   fn is_error_state(state: &Self) -> bool {
      *state == PoiTileState::ErrorState
   }

   fn get_cost(state: &Self) -> FloatOrdZero {
      match state {
         PoiTileState::SolidReachable => 1.0.into(),
         PoiTileState::LooseReachable => 10.0.into(),
         PoiTileState::Liquid => f32::INFINITY.into(),
         PoiTileState::SolidUnreachable => f32::INFINITY.into(),
         PoiTileState::LooseUnreachable => f32::INFINITY.into(),
         PoiTileState::Pitfall => f32::INFINITY.into(),
         PoiTileState::Spawn => f32::INFINITY.into(),
         PoiTileState::PoiMinor => f32::INFINITY.into(),
         PoiTileState::PoiMajor => f32::INFINITY.into(),
         PoiTileState::NoState => f32::INFINITY.into(),
         PoiTileState::ErrorState => f32::INFINITY.into(),
      }
   }
}

impl Default for Tile<PoiTileState> {
   fn default() -> Self {
      Tile {
         translation: Vec3::ZERO,
         neighbours: [None, None, None, None, None, None],
         state: PoiTileState::default(),
      }
   }
}

pub struct PoiBuilder {
   // randomnes related
   poi_rng: ThreadRng,

   // generator parameters
   poi_map_size: u8,
   poi_tile_count: usize, // for sync between container sizes

   // generator containers
   poi_tiles: HashMap<Idx2d, Tile<PoiTileState>>,
   poi_minors: Vec<Idx2d>,
}

impl Default for PoiBuilder {
   fn default() -> Self {
      PoiBuilder {
         poi_rng: rand::thread_rng(),
         poi_map_size: 0,
         poi_tile_count: 0,
         poi_tiles: HashMap::new(),
         poi_minors: Vec::new(),
      }
   }
}

impl PoiBuilder {
   pub fn new() -> PoiBuilder {
      PoiBuilder::default()
   }
   /// Function provides map size as count of the most extended hexagon tiles
   /// on the hexagon-like map shape.
   ///
   /// # Panicking
   /// size max value is 15, otherwise function panics.
   pub fn with_map_size(mut self, size: u8) -> Self {
      self.poi_map_size = size;
      let size_square = size.checked_mul(size).expect("Number too large!");
      self.poi_tile_count = 9 * size_square as usize - 15 * size as usize + 7;
      self
   }

   pub fn pre_generate(mut self) -> Self {
      // init tiles container
      let frozen_keys = self.generate_logical_positions();
      self.poi_tiles = frozen_keys
         .iter()
         .map(|idx2d| (idx2d.to_owned(), Tile::<PoiTileState>::default()))
         .collect();
      for (idx2d, tile) in self.poi_tiles.iter_mut() {
         // generate translations
         tile.translation = PoiBuilder::logical_to_path_vertex(&idx2d);

         // validate neighbours
         tile.neighbours = NEIGHBOUR_LOGICAL_POS_OFFSETS
            .clone()
            .iter()
            .map(|offset| {
               let neighbour = offset.to_owned() + idx2d.to_owned();
               frozen_keys.contains(&neighbour).then_some(neighbour)
            })
            .collect::<Vec<Option<Idx2d>>>()
            .try_into()
            .unwrap();
      }
      // mark POI central point
      self
         .poi_tiles
         .entry(POI_MAP_TOWN_INDEX)
         .and_modify(|tile| tile.state = PoiTileState::PoiMajor);
      // mark POI spawn points
      for idx in POI_MAP_SPAWN_POINTS {
         self
            .poi_tiles
            .entry(idx)
            .and_modify(|tile| tile.state = PoiTileState::Spawn);
      }
      // select poi minor point location TODO: add constraint for neighbourship distance
      {
         let first_poi_minor =
            self.poi_rng.next_u32() as usize % POI_MAP_SPECIALS_POSSIBLE_INDEXES.len();
         self
            .poi_minors
            .push(POI_MAP_SPECIALS_POSSIBLE_INDEXES[first_poi_minor]);
         self.poi_minors.push(
            POI_MAP_SPECIALS_POSSIBLE_INDEXES
               [(first_poi_minor + 8) % POI_MAP_SPECIALS_POSSIBLE_INDEXES.len()],
         );
         self.poi_minors.push(
            POI_MAP_SPECIALS_POSSIBLE_INDEXES
               [(first_poi_minor + 16) % POI_MAP_SPECIALS_POSSIBLE_INDEXES.len()],
         );
         self
            .poi_tiles
            .entry(self.poi_minors[0])
            .and_modify(|tile| tile.state = PoiTileState::PoiMinor);
         self
            .poi_tiles
            .entry(self.poi_minors[1])
            .and_modify(|tile| tile.state = PoiTileState::PoiMinor);
         self
            .poi_tiles
            .entry(self.poi_minors[2])
            .and_modify(|tile| tile.state = PoiTileState::PoiMinor);
      }
      self
   }

   pub fn generate(mut self) -> Self {
      wave_function_collapse(&mut self.poi_tiles, &mut self.poi_rng);
      fix_wave_errors(&mut self.poi_tiles);
      self
   }

   pub fn verify(mut self) -> Self {
      // temporary closure for pathfinding possibility
      let pathfind_test = |start: Idx2d, stop: Idx2d| -> bool {
         astar(
            &start,
            |idx| {
               self.poi_tiles[idx]
                  .neighbours()
                  .0
                  .iter()
                  .map(|&p| (p, PoiTileState::get_cost(&self.poi_tiles[&p].state)))
                  .collect::<Vec<(Idx2d, FloatOrdZero)>>()
            },
            |&idx| {
               self.poi_tiles[&idx]
                  .translation()
                  .distance(self.poi_tiles[&stop].translation())
                  .into()
            },
            |&p| p == stop,
         )
         .is_some()
      };
      //self.poi_tiles
      let poi_major_and_minor_tests = vec![
         (self.poi_minors[0], POI_MAP_TOWN_INDEX),
         (self.poi_minors[1], POI_MAP_TOWN_INDEX),
         (self.poi_minors[2], POI_MAP_TOWN_INDEX),
         (self.poi_minors[0], self.poi_minors[1]),
         (self.poi_minors[1], self.poi_minors[2]),
         (self.poi_minors[2], self.poi_minors[0]),
      ];
      let begin_tests = vec![
         (POI_MAP_SPAWN_POINTS[0], self.poi_minors[0]),
         (POI_MAP_SPAWN_POINTS[1], self.poi_minors[0]),
         (POI_MAP_SPAWN_POINTS[2], self.poi_minors[0]),
         (POI_MAP_SPAWN_POINTS[3], self.poi_minors[0]),
         (POI_MAP_SPAWN_POINTS[4], self.poi_minors[0]),
         (POI_MAP_SPAWN_POINTS[5], self.poi_minors[0]),
      ];
      let mut tests_return = false;
      for (start, stop) in begin_tests {
         tests_return |= pathfind_test(start, stop);
      }
      for (start, stop) in poi_major_and_minor_tests {
         tests_return &= pathfind_test(start, stop);
      }
      if tests_return {
         info!("Map is clear");
      }

      self
   }

   ///Builds map tiles from previously provided parameters
   ///and generated steps.
   ///Returns tile states and their positions
   pub fn build(self) -> HashMap<Idx2d, Tile<PoiTileState>> {
      self.poi_tiles
   }

   fn generate_logical_positions(&self) -> Vec<Idx2d> {
      let start_pos = Idx2d(
         0,
         -4 * (self.poi_map_size as i8).checked_sub(1).unwrap_or_default(),
      );
      let mut logical_positions = Vec::<Idx2d>::new();
      // Step1: Generate bottom tiles
      let mut row_base_pos = start_pos.clone();
      for n in 0..self.poi_map_size {
         let mut row_pos = row_base_pos.clone();
         let row_item_count = 3 * n + 1;
         for _ in 0..row_item_count {
            // Put new position in vector
            logical_positions.push(row_pos.clone());

            // Update new position for another tile
            row_pos += Idx2d(-2, 0);
         }
         // Update base position for new row
         row_base_pos += Idx2d(3, 2);
      }
      // Step2: Generate middle tiles
      {
         let mut row_base_pos = row_base_pos.clone() + Idx2d(-4, 0);
         let row_item_count = (3 * self.poi_map_size).checked_sub(3).unwrap_or_default();
         let iterations = self.poi_map_size.checked_sub(1).unwrap_or_default();
         for _ in 0..iterations {
            let mut row_pos = row_base_pos.clone();
            for _ in 0..row_item_count {
               logical_positions.push(row_pos.clone());
               row_pos += Idx2d(-2, 0);
            }
            row_base_pos += Idx2d(0, 4);
         }
      }
      row_base_pos += Idx2d(-3, 2);
      let row_item_count = (3 * self.poi_map_size).checked_sub(2).unwrap_or_default();
      let iterations = self.poi_map_size.checked_sub(2).unwrap_or_default();
      for _ in 0..iterations {
         let mut row_pos = row_base_pos.clone();
         for _ in 0..row_item_count {
            logical_positions.push(row_pos.clone());
            row_pos += Idx2d(-2, 0);
         }
         row_base_pos += Idx2d(0, 4);
      }
      // Step3: Generate top tiles
      for n in (0..self.poi_map_size).rev() {
         let mut row_pos = row_base_pos.clone();
         let row_item_count = 3 * n + 1;
         for _ in 0..row_item_count {
            // Put new position in vector
            logical_positions.push(row_pos.clone());

            // Update new position for another tile
            row_pos += Idx2d(-2, 0);
         }
         row_base_pos += Idx2d(-3, 2);
      }
      logical_positions
   }

   fn logical_to_path_vertex(logical: &Idx2d) -> Vec3 {
      Vec3::new(
         FRAC_SQRT_3_2 * logical.0 as f32,
         0f32,
         0.75f32 * logical.1 as f32,
      )
   }
}
