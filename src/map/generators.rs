pub mod poi;
use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::{
   collections::HashMap,
   ops::{Add, AddAssign},
};

use bevy::prelude::{info, Component, Vec3};
use bevy::utils::FloatOrd;
use pathfinding::num_traits::Zero;
use rand::{rngs::ThreadRng, RngCore};

const NEIGHBOUR_LOGICAL_POS_OFFSETS: [Idx2d; 6] = [
   Idx2d(-1, 2),
   Idx2d(-2, 0),
   Idx2d(-1, -2),
   Idx2d(1, -2),
   Idx2d(2, 0),
   Idx2d(1, 2),
];
// Thanks to [Wolfram Alpha answer](https://www.wolframalpha.com/input?i=sqrt%283%29%2F2)
const FRAC_SQRT_3_2: f32 = 0.8660254037844386467637231707529361834714026269051903140279034897;

#[derive(Component, Clone, Copy, Eq, Hash, PartialEq, Debug, Ord, PartialOrd)]
pub struct Idx2d(i8, i8);

#[derive(Component, Clone)]
pub struct NeighbourIndexes(pub Vec<Idx2d>);

#[derive(Copy, Ord, Eq, PartialEq, PartialOrd, Clone)]
pub struct FloatOrdZero(pub FloatOrd);

impl Add for FloatOrdZero {
   type Output = FloatOrdZero;

   fn add(self, rhs: Self) -> Self::Output {
      FloatOrdZero(FloatOrd(self.0 .0 + rhs.0 .0))
   }
}

impl Zero for FloatOrdZero {
   fn zero() -> Self {
      FloatOrdZero(FloatOrd(0.0))
   }

   fn is_zero(&self) -> bool {
      self.0 .0 == 0.0
   }
}

impl From<f32> for FloatOrdZero {
   fn from(value: f32) -> Self {
      FloatOrdZero(FloatOrd(value))
   }
}

#[derive(Clone, Component)]
pub struct Tile<T>
where
   T: WaveState + PartialEq + Clone,
{
   translation: Vec3,
   neighbours: [Option<Idx2d>; 6],
   state: T,
}

impl AddAssign for Idx2d {
   fn add_assign(&mut self, rhs: Self) {
      self.0 += rhs.0;
      self.1 += rhs.1;
   }
}

impl Add for Idx2d {
   type Output = Idx2d;

   fn add(self, rhs: Self) -> Self::Output {
      Idx2d(self.0 + rhs.0, self.1 + rhs.1)
   }
}

impl Display for Idx2d {
   fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
      write!(f, "({}, {})", self.0, self.1)
   }
}

impl Idx2d {
   pub fn into_path_vertex(&self) -> Vec3 {
      Vec3::new(FRAC_SQRT_3_2 * self.0 as f32, 0f32, 0.75f32 * self.1 as f32)
   }
}

impl<T> Tile<T>
where
   T: WaveState + PartialEq + Clone,
{
   pub fn translation(&self) -> Vec3 {
      self.translation.clone()
   }
   pub fn neighbours(&self) -> NeighbourIndexes {
      let mut shrinked = self.neighbours.to_vec();
      shrinked.retain(|neighbour| neighbour.is_some());
      NeighbourIndexes(
         shrinked
            .into_iter()
            .map(|neighbour| neighbour.unwrap())
            .collect::<Vec<Idx2d>>(),
      )
   }
   pub fn state(&self) -> T {
      self.state.clone()
   }
}

pub trait WaveState {
   fn possible_neighbour_states(state: &Self) -> Vec<Self>
   where
      Self: Sized;
   fn is_observed(state: &Self) -> bool;
   fn error_state() -> Self;
   fn is_error_state(state: &Self) -> bool;
   fn get_cost(state: &Self) -> FloatOrdZero;
}

fn get_remaining_tiles<T>(tile_map: &HashMap<Idx2d, Tile<T>>) -> usize
where
   T: WaveState + PartialEq + Clone,
{
   tile_map
      .to_owned()
      .into_values()
      .filter(|tile| !T::is_observed(&tile.state))
      .count()
}

fn wave_function_collapse<T>(tile_map: &mut HashMap<Idx2d, Tile<T>>, rng: &mut ThreadRng)
where
   T: WaveState + PartialEq + Clone,
{
   loop {
      // acquire computing conditions
      if get_remaining_tiles(tile_map) == 0 {
         break;
      }
      // main function collapse loop
      let mut requested_collapse = HashMap::<Idx2d, Vec<T>>::new();
      for (idx2d, tile) in tile_map.iter() {
         if !T::is_observed(&tile.state) {
            let mut valid_states = Vec::<T>::new();
            let mut was_once_empty = false;
            for neighbour in &tile.neighbours {
               if let Some(neighbour_idx) = neighbour {
                  if T::is_observed(&tile_map[&neighbour_idx].state) {
                     let mut possible_states =
                        T::possible_neighbour_states(&tile_map[&neighbour_idx].state);
                     if was_once_empty {
                        valid_states.retain(|state| possible_states.contains(state))
                     } else {
                        valid_states.append(&mut possible_states);
                        was_once_empty = !valid_states.is_empty();
                     }
                  }
               }
            }
            if valid_states.is_empty() {
               if was_once_empty {
                  valid_states.push(T::error_state());
                  requested_collapse.insert(idx2d.to_owned(), valid_states);
               }
            } else {
               requested_collapse.insert(idx2d.to_owned(), valid_states);
            }
         }
      }
      if requested_collapse.len() == 0 {
         break;
      }
      for (index, states) in requested_collapse {
         tile_map
            .entry(index)
            .and_modify(|tile| tile.state = states[rng.next_u32() as usize % states.len()].clone());
      }
   }
}

fn get_error_state_indexes<T>(tile_map: &HashMap<Idx2d, Tile<T>>) -> Vec<Idx2d>
where
   T: WaveState + PartialEq + Clone,
{
   tile_map
      .to_owned()
      .into_keys()
      .filter(|key| T::is_error_state(&tile_map[key].state))
      .collect()
}

fn fix_wave_errors<T>(tile_map: &mut HashMap<Idx2d, Tile<T>>)
where
   T: WaveState + PartialEq + Eq + Clone + Hash + Debug,
{
   // repeat until all cleared
   loop {
      let error_states = get_error_state_indexes(tile_map);
      if error_states.len() == 0 {
         break;
      }

      for nth_state in error_states {
         // get possible candidates for this state
         let mut candidates = HashMap::<T, u8>::new();
         for neighbour in &tile_map[&nth_state].neighbours {
            if let Some(neighbour) = neighbour {
               for vote in T::possible_neighbour_states(&tile_map[&neighbour].state) {
                  let candidate = candidates.entry(vote).or_insert(0);
                  *candidate += 1;
               }
            }
         }
         // promote selected candidate
         let new_state = candidates
            .iter()
            .max_by(|a, b| a.1.cmp(&b.1))
            .map(|(key, _)| key);
         if let Some(new_state) = new_state {
            tile_map
               .entry(nth_state.clone())
               .and_modify(|val| val.state = new_state.to_owned());
         }
         // select tile to be changed after election
         for neighbour in tile_map[&nth_state].neighbours().0 {
            if !T::is_error_state(&tile_map[&neighbour].state) {
               if !T::possible_neighbour_states(&tile_map[&neighbour].state)
                  .contains(&tile_map[&nth_state].state)
               {
                  tile_map
                     .entry(neighbour.to_owned())
                     .and_modify(|val| val.state = T::error_state());
               }
            }
         }
      }
   }
}
