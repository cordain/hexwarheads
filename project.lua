local project = vim.api.nvim_create_augroup('hexwarheads', { clear = true })
vim.api.nvim_create_autocmd({ 'BufWritePost' }, {
  pattern = '*.rs',
  group = project,
  command = '!rustfmt <afile>',
})
